import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { AppRoute } from '~/helpers/app.route';

export interface HomeProps {}

const Home: React.SFC<HomeProps> = () => {
  const navigation = useNavigation();

  return (
    <page>
      <wrapLayout>
        <button text="Go Stack Screen" onTap={() => navigation['push'](AppRoute.Counter)} className="-outline" />
      </wrapLayout>
    </page>
  );
};

export default Home;
