import { useNavigation } from '@react-navigation/core';
import React from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { CounterAction } from '~/actions';
import { ReduxState } from '~/reducers';

export interface CounterProps {}

const Counter: React.SFC<CounterProps> = () => {
  const counter = useSelector<ReduxState, number>((state) => state.counter);

  const dispatch = useDispatch();
  const navigation = useNavigation();

  const handleDecrease = () => {
    dispatch(CounterAction.decrease(1));
  };

  const handleIncrease = () => {
    dispatch(CounterAction.increase(1));
  };

  return (
    <page>
      <wrapLayout>
        <button text="Border + Transparent Background" onTap={handleDecrease} className="-outline" />
        <label text={`${counter}`} className="h1" />
        <button text="Button" onTap={handleIncrease} className="-primary" />
        <button text="Go back Screen" onTap={() => navigation.goBack()} className="-outline" />
      </wrapLayout>
    </page>
  );
};

export default Counter;
