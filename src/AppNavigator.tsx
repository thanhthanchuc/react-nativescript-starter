import * as React from 'react';
import { BaseNavigationContainer } from '@react-navigation/core';
import { stackNavigatorFactory, tabsNavigatorFactory } from 'react-nativescript-navigation';
import { CounterPage, HomePage } from '~/screens';
import { CounterStack } from './helpers/app.route';

const TabNavigator = tabsNavigatorFactory();
const StackNavigator = stackNavigatorFactory<CounterStack>();

const CounterStackNavigator = () => {
  return (
    <StackNavigator.Navigator
      initialRouteName="Home"
      screenOptions={{
        headerShown: true,
      }}
    >
      <StackNavigator.Screen name="Counter" component={CounterPage} />
      <StackNavigator.Screen name="Home" component={HomePage} />
    </StackNavigator.Navigator>
  );
};

const TabsAppContainer = () => (
  <BaseNavigationContainer>
    <TabNavigator.Navigator initialRouteName="first">
      <TabNavigator.Screen name="first" component={CounterStackNavigator} />
      <TabNavigator.Screen name="second" component={CounterPage} />
    </TabNavigator.Navigator>
  </BaseNavigationContainer>
);

export default TabsAppContainer;
