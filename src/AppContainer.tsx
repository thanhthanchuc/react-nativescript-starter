import React from 'react';
import { Provider as ReduxProvider } from 'react-redux';
import { configureStore } from '@reduxjs/toolkit';

import AppNavigator from './AppNavigator';
import reducer from './reducers';

const AppContainer = () => {
  return (
    <ReduxProvider store={configureStore({ reducer })}>
      <AppNavigator />
    </ReduxProvider>
  );
};

export default AppContainer;
