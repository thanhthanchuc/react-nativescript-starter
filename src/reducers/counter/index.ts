import { createReducer } from '@reduxjs/toolkit';

import { CounterAction } from '~/actions';

const initState = 0;

const CounterReducer = createReducer(initState, {
  [CounterAction.increase.toString()]: (state, { payload }) => {
    return state + payload.quantity;
  },
  [CounterAction.decrease.toString()]: (state, { payload }) => {
    return state - payload.quantity;
  },
});

export default CounterReducer;
