export type CounterStack = {
  Counter: undefined;
  Home: { sort: 'latest' | 'top' } | undefined;
};

export const AppRoute = {
  Counter: 'Counter',
  Feed: 'Feed',
};
