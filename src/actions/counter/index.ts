import { createAction } from '@reduxjs/toolkit';

const INCREASE = 'INCREASE';
const DECREASE = 'DECREASE';

const increase = createAction(INCREASE, (quantity: number) => ({ payload: { quantity } }));
const decrease = createAction(DECREASE, (quantity: number) => ({ payload: { quantity } }));

export default { increase, decrease };
